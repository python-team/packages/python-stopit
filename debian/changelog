python-stopit (1.1.2-3) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

  [ Colin Watson ]
  * Make build run tests again following the removal of "setup.py test".
  * Don't rely on "setup.py test" in autopkgtest (closes: #1079761).
  * Use pybuild-plugin-pyproject.

 -- Colin Watson <cjwatson@debian.org>  Tue, 03 Sep 2024 13:58:22 +0100

python-stopit (1.1.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Jun 2022 12:22:34 -0400

python-stopit (1.1.2-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * d/patches/time-test.patch: Drop, applied upstream

  [ Ondřej Kobližek ]
  * Ignore .egg-info
  * New upstream release
  * Rebase patches
  * Bump standards version to 4.4.0 (no changes)
  * Bump debhelper compat level to 12 and use debhelper-compat
  * d/control:
    - Add python3-pkg-resources to Depends (Closes: #896319)
    - Add autopkgtest
  * d/tests: Add the Python 3 module test
  * Drop Python 2 support (Closes: #854542, #896297)
  * License update to expat

 -- Ondřej Kobližek <koblizeko@gmail.com>  Wed, 17 Jul 2019 00:57:28 +0200

python-stopit (1.1.1-1) unstable; urgency=medium

  * Initial release (Closes: #822922)

 -- Adrian Alves <aalves@gmail.com>  Fri, 29 Apr 2016 16:02:41 -0400
